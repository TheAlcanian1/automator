from discord import Message
from array import array
import discord
from discord import User
import subprocess
from discord.ext.commands import Bot
from discord.ext import commands
client = discord.Client()
import os
import logging
import os
import os.path
import re
import json

# # # Config part # # #

# find XDG config directory from environment variable. otherwise, return current user's home directory plus /.config
config_home_incomplete = os.environ.get('XDG_CONFIG_HOME', os.environ['HOME'] + '/.config')

# take the userconfigdir variable and attatch /programname/ to it to get the dir that programname should create
config_home_complete = config_home_incomplete + '/automator/'

# make program's config directory. if it already exists, don't panic. or exception.
try:
    os.mkdir(config_home_complete)
except FileExistsError:
    print('Config folder already exists at ' + config_home_complete + '.')
else:
    print('Config folder created at ' + config_home_complete + '.')

pathcheck_mainconfig = os.path.exists(config_home_complete + 'automator.json')
if pathcheck_mainconfig:
  with open(config_home_complete + 'automator.json') as json_data_file:
       data = json.load(json_data_file)
  pathcheck_autowordpunish = os.path.exists(config_home_complete + 'automator_autowordpunish.json')
  if pathcheck_autowordpunish:
    with open(config_home_complete + 'automator_autowordpunish.json') as json_data_file:
         data_autowordpunish = json.load(json_data_file)
    pathcheck_autoroleadd = os.path.exists(config_home_complete + 'automator_autoroleadd.json')
    if pathcheck_autoroleadd:
      with open(config_home_complete + 'automator_autoroleadd.json') as json_data_file:
                     data_autoroleadd = json.load(json_data_file)
                     
        
    else:
        print('Uh oh! Automator could not start because you\'re missing a config file! Create ' + config_home_complete + 'automator_autoroleadd.json and fill it with the contents of a sample autoroleadd file from the repo! ')
        exit()
  else:
      print('Uh oh! Automator could not start because you\'re missing a config file! Create ' + config_home_complete + 'automator_autowordpunish.json and fill it with the contents of a sample autowordpunish file from the repo!')
      exit()
else:
    print('Uh oh! Automator could not start because you\'re missing a config file! Create ' + config_home_complete + 'automator.json and fill it with this data:\n{\n   "token": "<bot account\'s token>"\n}')
    exit()
    
# # # Config part # # #



logging.basicConfig(level=logging.INFO)

bot = commands.Bot(command_prefix='>')

@bot.command()
async def commands(ctx, arg):
  await ctx.send('Automator Commands:\n`roledelete` : Deletes roles without anybody attatched to them.\n`messagesearchdelete x`: Searches for messages containing `x` and deletes them.\n')



                                            

@bot.command()
async def messagesearchdelete(ctx, arg):
  search = arg
  messagesearchdelete.messages_deleted = 0
  async def transform(message):
    print('test')
    match = re.search(search + '(?i)', message.content, flags=0)
  #  transform.messages_deleted = messagesearchdelete.messages_deleted + 1
    if match:
      messagesearchdelete.messages_deleted = messagesearchdelete.messages_deleted + 1
      await message.delete()
      print(messagesearchdelete.messages_deleted)
  messagesearchdelete.messages_deleted = -1                            
  async for elem in ctx.channel.history().map(transform): #.filter(predicate):
#    messages_deleted_true = messagesearchdelete.messages_deleted + 1
#    print(messages_deleted_true)
    pass
  embed=discord.Embed(title="Message Search Delete", color=0x00ff00)
  embed.add_field(name="Messages Deleted:", value=messagesearchdelete.messages_deleted, inline=True)
  embed.set_footer(text="Automator")
  await ctx.send(embed=embed)
  
@bot.event
async def on_ready():
  print('Ready!')

@bot.event
async def on_message(message):
    if message.author == bot.user:
      return
    word_number = -1
    unparsed_autowordpunishban_list = (data_autowordpunish["ban_words"])
    for str in unparsed_autowordpunishban_list:
      word_number = word_number + 1
      banwordsearch = re.search((data_autowordpunish["ban_words"][word_number]), message.content, flags=0)
      if banwordsearch:
        await message.guild.ban(message.author, delete_message_days=1, reason="Said a forbidden phrase")
        return
      else:
        pass
    word_number2 = -1
    unparsed_autowordpunishkick_list = (data_autowordpunish["kick_words"])
    for str in unparsed_autowordpunishkick_list:
      word_number2 = word_number2 + 1
      kickwordsearch = re.search((data_autowordpunish["kick_words"][word_number2]), message.content, flags=0)
      if kickwordsearch:
        await message.guild.kick(message.author, reason="Said a forbidden phrase")
        return
      else:
        pass
    word_number3 = -1
    unparsed_autowordpunishmute_list = (data_autowordpunish["mute_words"])
    for str in unparsed_autowordpunishmute_list:
      word_number3 = word_number3 + 1
      mutewordsearch = re.search((data_autowordpunish["mute_words"][word_number2]), message.content, flags=0)
      if mutewordsearch:

        if data_autowordpunish.get("mute_role") == None:
          # muterole = discord.utils.get(message.guild.roles, id=(data_autowordpunish["mute_role"]))
            user = message.author
            muterole2 = int((data_autowordpunish["mute_role"]))
            muterole3 = message.guild.get_role(muterole2) 
            print(muterole2)
            await user.add_roles(muterole3)
            return
        else:
            muterole2 = discord.utils.get(message.guild.roles, name="Muted")
            user = message.author
            await user.add_roles(muterole2)
            return
      else:
        await bot.process_commands(message)
                                              
    
bot.run(data["token"])
