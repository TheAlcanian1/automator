# Automator

# Note: A lot of the things documented in this README are not present in Automator currently. I am working on implementing them.

# AUTOMATOR DOES NOT HAVE PERMISSION CHECKS CURRENTLY. USING AUTOMATOR ON A PUBLIC SERVER IS A **VERY** BAD IDEA. PLEASE DO NOT DO THIS.

An administration bot for Discord written in Python, meant to ease the pain on the staff of a server. Fully configurable.

**!! Automator will ONLY run under Linux (and maybe macOS, give it a shot) !!**

Automator uses the discord.py library. Install this how you please ([instructions here](https://discordpy.readthedocs.io/en/latest/intro.html)).

Automator has three configuration files, being placed in a folder named `automator` inside the location defined in your `XGD_CONFIG_HOME` environment variable. On typical Linux systems, this is `/home/user/.config`.
Automator will assume you have a `/home/user/.config` if you do not have `XGD_CONFIG_HOME` assigned. 

These files are `automator.json`, `automator_autowordpunish.json` and `automator_autoroleadd.json`.
Automator can *only* read from these files, and cannot write them. 

`automator.json` contains general settings, such as the permissions required to use a command, if the command is enabled, and other settings.

`automator_autowordpunish.json` contains the list of words for `autowordpunish` and their respective punishments.

`automator_autoroleadd.json` contains the list of roles to add to users and their respective phrases. While you *can* use `autowordpunish` to achieve a basic version of `autoroleadd`, you can only define one role to add with `autowordpunish`. `autoroleadd` allows you to use seperate roles for each phrase.

More info and the syntax of each is documented in the Wiki.
